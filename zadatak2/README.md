# Locastic Frontend Task 2


### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### NOTES
Built a simple form validation using Constraint validation API, leveraging HTML5 validation. We assume best case scenarios (such as that all fields are available) and form validation kicks in on form submission. For data sending we use the Fetch API coupled with Promises to handle status codes different from 2XX. Imported polyfill for browsers that don't support Fetch API.
