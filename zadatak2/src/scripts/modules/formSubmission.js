import FormValidator from './formValidator';
// import fetch polyfill
import 'whatwg-fetch';

export default function submitForm(formId) {
	const form = document.getElementById(formId);
	const formValidator = new FormValidator(form);

	// on form submission do custom validation & fetch call
	form.addEventListener('submit', function (event) {
		event.preventDefault();
		_doFormSubmit();
	}, false);

	function _doFormSubmit() {
	  if (formValidator.validate()) {
		  window.fetch(form.action, {
	      method: form.method,
	      body: new FormData(form)
	    })
	    .then(res => {
				if (res.ok) {
				  console.log(`Yaaay, fetch success! ${res}`);
				} else {
				  console.log('HTTP error', res.status, res.statusText);
				  form.querySelector('[role=alert]').hidden = false;
				  }
	    })
	    .catch(err => {
	    	console.log('HTTP error', err.status, err.statusText);
	      form.querySelector('[role=alert]').hidden = false;
	    });

	    // hide the error message on form submission
		  form.querySelector('[role=alert]').hidden = true;
	  }
	}
}
