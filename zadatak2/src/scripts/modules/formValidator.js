export default class FormValidator {
	constructor(form) {
		console.log('FormValidator class called.');
		this.form = form;
		this.fields = this.form.elements;
		FormValidator._addEventListeners(this.form, this.fields);
	}

	// basic validation function
	validate() {
		return this.fields['name'].validity.valid && this.fields['email'].validity.valid && this.fields['message'].validity.valid;
	}

	// private function for custom event listeners & error messages
	static _addEventListeners(form, fields) {
		fields.forEach(f => {
			f.addEventListener('input', function () {
				if (f.type === 'text') {
					if (f.validity.valueMissing) {
						f.setCustomValidity('Enter your name');
					} else if (f.validity.tooShort) {
						f.setCustomValidity(`Name has to contain at least ${f.minLength} characters`);
					} else {
						f.setCustomValidity('');
						f.valid = true;
					}
				} else if (f.type === 'email') {
					if (f.validity.typeMismatch) {
						f.setCustomValidity('Gimme valid email');
					} else {
						f.setCustomValidity('');
					}
				} else if (f.type === 'textarea') {
					if (f.validity.valueMissing) {
						f.setCustomValidity('Enter your message');
					} else if (f.validity.tooShort) {
						f.setCustomValidity(`Message has to contain at least ${f.minLength} characters`);
					} else {
						f.setCustomValidity('');
						f.valid = true;
					}
				}
			}, false);
		});
	}
}
