module.exports = {
  plugins: {
    "autoprefixer": {},
    "postcss-pxtorem": {
    	propList: ['font', 'font-size', 'line-height', 'letter-spacing', 'padding', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'width', 'height', 'max-width']
    }
  }
};
