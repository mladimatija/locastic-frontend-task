import Stickyfill from 'stickyfilljs';

export default function menu(
	triggerElementClass = 'js-toggle-nav',
	menuEl = '.menu',
	mobileMenuActiveClass = 'mm-is-active',
	viewportWidthTrigger = 1024
	) {
		// for simplicity we assume best case scenarios
		const body = document.body;
		const menu = document.querySelector(menuEl);
		const mmTriggers = document.getElementsByClassName(triggerElementClass);
		// get initial viewport width
		let viewportWidth = window.innerWidth || document.documentElement.clientWidth;

	  // sticky polyfill
	  const stickyElements = document.querySelectorAll('.sticky');
	  Stickyfill.add(stickyElements);

	  if (mmTriggers.length) {
		  // trigger helper function on mobile menu icon button click
		  [...mmTriggers].forEach(function(mmTrigger) {
		  	mmTrigger.addEventListener('click', function() {
		  		_triggerMobileMenu(this);
		  	}, false);
		  });

			// deactivate mobile menu on desktop resolutions
			window.addEventListener('resize', () => {
				viewportWidth = window.innerWidth || document.documentElement.clientWidth;
				if (viewportWidth >= 1024 && body.classList.contains(mobileMenuActiveClass)) body.classList.remove(mobileMenuActiveClass);
			});

			// close mobile menu on ESC press
			window.addEventListener('keydown', _logKey);
		}

		function _triggerMobileMenu(open = true) {
			viewportWidth < viewportWidthTrigger && open && !body.classList.contains(mobileMenuActiveClass) ? body.classList.add(mobileMenuActiveClass) : body.classList.remove(mobileMenuActiveClass);
		}

		function _logKey(e) {
    	const event = e || window.event;
    	const key = event.key || event.keyCode;

	    if (key === 'Escape' || key === 'Esc' || key === 27) {
	      _triggerMobileMenu(false);
	    }
		};
	}
