/* IMPORT DEPENDENCIES */
import {tns} from 'tiny-slider/src/tiny-slider';

export default function slider() {
	const sliderEl = document.getElementsByClassName('slider');
	if (sliderEl.length) {
		[...sliderEl].forEach(s => {
			const slider = tns({
				container: s.querySelector('.slider-container'),
				items: 1,
				slideBy: 1,
				autoplay: true,
				controls: false,
				navPosition: 'bottom',
				autoplayButtonOutput: false,
				center: true
			});
		});
	}
}
