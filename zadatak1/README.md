# Locastic Frontend Task 1


### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### NOTES

* **[webpack-starter](https://github.com/wbkd/webpack-starter/)** package korišten kao starting point projekta
* **[sass-mq](https://github.com/sass-mq/sass-mq)** package korišten za responsive breakpoints (ems)
* **[typi](https://github.com/zellwk/typi)** package korišten za definiranje typography mapova. Korištena samo inicijalna konfiguracija, kasnije skužio da je projekt premalen da opravda kompliciranje tipografije.
* **[postcss-pxtorem](https://github.com/sass-mq/sass-mq)** package korišten za konverziju pixela u rem-ove. Zaboravih na početku napravit sass funkciju za automatsku konverziju pa mi se kasnije nije dalo s time zezati, lakše ovako :)
* **[stickyfill](https://github.com/wilddeer/stickyfill)** package korišten kao position: sticky polyfill
* **[tiny-slider](https://github.com/ganlanyuan/tiny-slider)** package korišten za slider

1. Nije pixel perfect, uočio sam nekoliko nekonzistentnosti kod razmaka, posebice u headeru pa sam u tim slučajevima uzeo srednju vrijednost.
2. Funkcionalnost skaliranja slikovnih elemenata svedena na minimum.
3. Slider ima minimalnu funkcionalnost (samo autoplay).
4. Nema accessibilitya.
5. Moguć scroll stranice dok je otvoren mobile menu.
6. Nisam koristio nikakav grid, činilo mi se reduntantno za par komponenti raditi grid ili koristiti kakav plugin za to. Gdje je potrebno koristi se max-width property.
