# Locastic Frontend Task 4

Razvijate single-page web aplikaciju u AngularJS-u ili nekom drugom JS frameworku. Komunikacija sa serverskom podrškom je pozadinska (AJAX). Da bi korisnik koristio vašu aplikaciju, mora se izvršiti uspješna
autentifikacija nakon čega API vraća autorizacijski token (ograničene
vremenske valjanosti) koji se koristi za sve ostale API pozive.


### 1. Kako bi pohranili autorizacijski token u vašoj aplikaciji?
Najčešći slučaj jest pohrana autorizacijskih tokena u cookie ili local storage no pri tome se izlažemo mogućnostima malicioznih napada od treće strane. Iako je pohrana u cookie bolja solucija od navedenih, bolje je autorizacijske tokene pohranjivati in-memory u aplikaciji. Prema tome, ako korisnik zatvori aplikaciju autorizacijski token se briše i korisnik je odjavljen iz aplikacije. Isti se dešava on session timeout, ako se jedan token koristi za autorizaciju korisnika i validaciju valjanosti sesije.

### 2. Kako biste osigurali sigurnu vezu prema serveru u slučaju da promet ne ide preko HTTPS protokola? Što biste promijenili u slučaju da koristite HTTPS vezu?
Enkripcijom podataka koji se šalju na backend nekim od encryption librarya korištenjem public keya dostupnog web aplikaciji, a gdje bi se na backend strani dekriptirao sadržaj poziva korištenjem valjanog private keya. Za dodatnu sigurnost možebitno koristiti i keysetove.

Ako je pitanje vezano uz to što bi promjenio u gore navedenom slučaju funkcioniranja aplikacije, definitivno bi dodao nešto poput Refresh Tokena, koji bi služio za revalidaciju autorizacijskog tokena bez potrebe da se korisnika aplikacije ponovno o tome obavijesti (npr. ako se dogodio session timeout i korisnik nakon toga ponovno krene u interakciju sa aplikacijom).

### 3. Kako bi invalidirali token u slučaju isteka njegove valjanosti, a kako u slučaju odjave korisnika (logout)?
U slučaju frontenda, na odjavu korisnika token se jednostavno obriše sa client-sidea dok u slučaju isteka možemo usporediti expiration time tokena sa trenutnim vremenom (kada se radi poziv prema API-u).

U slučaju backenda, token jednostavno možemo staviti na blacklistu ili ako npr. koristimo JWT promjeniti tokenu u pitanju secret s kojim je potpisan (JWS) čime će token automatski biti nevaljan.

Ovisno o tome želimo li prilikom isteka njegove valjanosti token uništiti ili ostaviti mogućnost revalidacije, mogu se koristiti različiti pristupi, jel.

### 4. Koje alternative predlažete za osiguravanje autoriziranog pristupa API-ju osim autorizacijskim tokenom?
* HTTP Basic Authentication (username i password), ako se komunikacija odvija koristi HTTPS,
* API key organičen na korištenje s određenih IP adresa,
* OAuth

### 5. Prokomentirajte potpisivanje API poziva autorizacijskim tokenom na sigurnoj i nesigurnoj vezi (odlučite se za jedan algoritam i tehniku “digitalnog potpisivanja” zahtjeva).